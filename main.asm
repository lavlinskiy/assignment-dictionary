%include "lib.inc"
%include "dict.asm"
%include "words.inc"

%define buffer_size 256
%define stdout 1
%define stderr 2

global _start

section .rodata

enter_key: db "Enter key: ", 10, 0x0


no_matches_err: db "No matches", 10, 0x0
too_long_err: db "Too long", 10, 0x0

section .text
_start:
	mov rdi, enter_key
	push rbx
	mov rbx, stderr
	call print_string
	pop rbx

	sub rsp, buffer_size
 	mov rdi, rsp
 	mov rsi, buffer_size
	call read_word

 	cmp rax, 0
	je .too_long
 	mov rdi, rsp
	mov rsi, head
 	call find_word
	add rsp, buffer_size
	cmp rax, 0
 	je .no

.yes:
        add rax, rdx
        push rbx
        mov rbx, stderr
        call print_string
        pop rbx
	xor rdi, rdi
        call exit

.too_long:

	add rsp, buffer_size
	mov rdi, too_long_err
	jmp .error_exit
 .no:
	mov rdi, no_matches_err
.error_exit:
	push rbx
        mov rbx, stderr
        call print_string
        pop rbx
        mov rdi, 1
        call exit


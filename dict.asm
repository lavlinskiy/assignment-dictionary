%include "lib.inc"

%define label_size 8

section .text

; param 1: rdi, pointer to string
; param 2: rsi, pointer to list

find_word:
	push rdi
	push rsi
.loop:
	add rsi, label_size
	push rsi
	push rdi
	call string_equals
	pop rdi
	pop rsi

 	cmp rax, 1
	je .yes
	mov rsi, [rsi - label_size]
	cmp rsi, 0
	jne .loop
.no:
 	xor rax, rax
	jmp .exit
.yes:
 	mov rax, rsi
.exit:
	pop rsi
	pop rdi
	ret

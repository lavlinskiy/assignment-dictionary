ELF = -f elf64
nasm = nasm
all: main


main: main.o lib.o dict.o
	ld -o $@ $^



main.o: main.asm words.inc colon.inc
	$(nasm) $(ELF) -o $@ $<



dict.o: dict.asm lib.o
	$(nasm) $(ELF) -o $@ $<



lib.o: lib.asm
	$(nasm) $(ELF) -o $@ $<

clean:
	rm ./main
	rm *.o

